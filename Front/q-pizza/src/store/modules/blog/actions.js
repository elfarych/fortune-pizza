import axios from 'axios'

export async function loadPosts ({ commit }) {
  try {
    await axios.get(`${this.getters.getServerURL}/blog/posts/`)
      .then(response => commit('mutationPosts', response.data.results))
  } catch (e) {
    console.log(e)
  }
}

export async function loadPost ({ commit }, slug) {
  try {
    await axios.get(`${this.getters.getServerURL}/blog/posts/${slug}/`)
      .then(response => commit('mutationPost', response.data))
  } catch (e) {
    console.log(e)
  }
}

export async function init ({ dispatch }) {
  return dispatch('loadPosts')
}
