export function mutationIsInit (state) {
  state.isInit = true
}

export function mutationPost (state, data) {
  state.post = data
}

export function mutationPosts (state, data) {
  state.homePosts = data.filter(item => item.show_on_home_page)
  state.posts = data
}
