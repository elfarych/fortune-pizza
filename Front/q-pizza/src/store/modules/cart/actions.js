import { Notify } from 'quasar'
import notifier from 'src/utils/notifier'

export function init ({ commit, dispatch }) {
  const items = JSON.parse(localStorage.getItem('cart')) || []
  commit('mutationCartLen', items.length)
  commit('mutationCartItems', items)
  commit('mutationIsInit')
}

export function getCartLen ({ state, commit }) {
  commit('mutationCartLen', state.items.length)
}

export function addToCard ({ commit, state, dispatch }, product) {
  const vm = this
  const items = JSON.parse(localStorage.getItem('cart')) || []
  Notify.create({
    message: `${product.title} теперь в вашей корзине`,
    color: 'positive',
    position: 'top',
    avatar: `${product.miniature}`,
    actions: [
      { label: 'В корзину', color: 'yellow', handler: () => { vm.$router.push('/shop/cart') } },
      { icon: 'close', color: 'white' }
    ]
  })
  product.cartId = `${new Date().getMilliseconds()} + ${new Date().getSeconds()}`
  product.quantity = 1
  items.push(product)
  localStorage.setItem('cart', JSON.stringify(items))
  commit('mutationCartItems', items)
  dispatch('getCartLen')
}

export function deleteProductFromCart ({ commit, dispatch, state }, product) {
  const items = JSON.parse(localStorage.getItem('cart'))
  items.splice(state.items.findIndex(item => item.cartId === product.cartId), 1)
  notifier('Товар удален из корзины.')
  localStorage.setItem('cart', JSON.stringify(items))
  commit('mutationCartItems', items)
  commit('mutationCartLen', items.length)
  commit('mutationCartItems', items)
  dispatch('getCartLen')
}
