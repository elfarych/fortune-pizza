export function mutationIsInit (state) {
  state.isInit = true
}

export function mutationCartLen (state, data) {
  state.len = data
}

export function mutationCartItems (state, data) {
  state.items = data
}

export function mutationCartSum (state, data) {
  state.sum = data
}
