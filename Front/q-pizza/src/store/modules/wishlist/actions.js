import notifier from 'src/utils/notifier'
import { Notify } from 'quasar'

export function init ({ commit, dispatch }) {
  const items = JSON.parse(localStorage.getItem('wishlist')) || []
  commit('mutationWishListItems', items)
  dispatch('getLen')
  commit('mutationIsInit')
}

export function getLen ({ state, commit }) {
  commit('mutationWishlistLen', state.items.length)
}

export function addProduct ({ commit, state, dispatch }, product) {
  const vm = this
  const items = JSON.parse(JSON.stringify(state.items)) || []
  if (items.find(item => item.id === product.id)) {
    notifier('Товар удален из избранного')
    items.splice(items.findIndex(item => item.id === product.id), 1)
  } else {
    Notify.create({
      message: `${product.title} теперь в избранном`,
      color: 'positive',
      position: 'top',
      avatar: `${product.miniature}`,
      actions: [
        { label: 'В избранное', color: 'yellow', handler: () => { vm.$router.push('wishlist') } },
        { icon: 'close', color: 'white' }
      ]
    })
    items.push(product)
  }
  localStorage.setItem('wishlist', JSON.stringify(items))
  commit('mutationWishListItems', items)
  dispatch('getLen')
}
