export function mutationIsInit (state) {
  state.isInit = true
}

export function mutationWishlistLen (state, data) {
  state.len = data
}

export function mutationWishListItems (state, data) {
  state.items = data
}
