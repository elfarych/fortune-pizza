export function mutationIsInit (state) {
  state.isInit = true
}

export function mutationCategories (state, data) {
  state.categories = data
}

export function mutationHotProducts (state, data) {
  state.hotProducts = data
}

export function mutationCategory (state, data) {
  state.category = data
}

export function mutationProduct (state, data) {
  data.price = parseInt(data.price)
  data.variations.forEach(item => { item.price = parseInt(item.price) })
  data.additions.forEach(item => { item.price = parseInt(item.price) })
  state.product = data
  state.product.selectedVariation = data.variations[0] || {}
  state.product.selectedAdditions = []
  state.product.total_price = data.price
  state.product.additionsTotalPrice = 0
}

export function mutationProductAdditions (state, { addition }) {
  if (state.product.selectedAdditions.find(item => item.id === addition.id)) {
    const index = state.product.selectedAdditions.findIndex(item => item.id === addition.id)
    state.product.additionsTotalPrice = state.product.additionsTotalPrice - addition.price
    state.product.selectedAdditions.splice(index, 1)
  } else {
    state.product.additionsTotalPrice += addition.price
    state.product.selectedAdditions.push(addition)
  }
  state.product.total_price = (Object.values(state.product.selectedVariation).length ? state.product.selectedVariation.price : state.product.price) + state.product.additionsTotalPrice
}

export function mutationProductVariation (state, variation) {
  state.product.selectedVariation = variation
  state.product.total_price = variation.price + state.product.additionsTotalPrice
}

export function mutationsCleanProductAdditions (state) {
  state.product.selectedAdditions = []
}

export function mutationsCleanProductVariations (state) {
  state.product.selectedVariation = {}
}

export function mutationProducts (state, data) {
  state.products = data
}

export function mutationLabels (state, data) {
  state.labels = data
}

export function mutationLabel (state, data) {
  state.label = data
}
