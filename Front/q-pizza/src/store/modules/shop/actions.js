import axios from 'axios'

export async function loadCategories ({ commit }) {
  try {
    await axios.get(`${this.getters.getServerURL}/shop/categories/`)
      .then(response => commit('mutationCategories', response.data.results))
  } catch (e) {

  }
}

export async function loadCategory ({ commit }, slug) {
  try {
    await axios.get(`${this.getters.getServerURL}/shop/category/${slug}/`)
      .then(response => {
        commit('mutationCategory', response.data)
        commit('mutationProducts', response.data.products)
      })
  } catch (e) {
    console.log(e)
  }
}

export async function loadHotProducts ({ commit }) {
  try {
    await axios.get(`${this.getters.getServerURL}/shop/products/?hit=true&maxCount=5`)
      .then(response => commit('mutationHotProducts', response.data.results))
  } catch (e) {
    console.log(e)
  }
}

export async function loadProduct ({ commit }, slug) {
  try {
    await axios.get(`${this.getters.getServerURL}/shop/product/${slug}/`)
      .then(response => {
        commit('mutationProduct', response.data)
        // commit('mutationShowProductDetail', true)
      })
  } catch (e) {
    console.log(e)
  }
}

export async function loadLabels ({ commit }) {
  try {
    await axios.get(`${this.getters.getServerURL}/shop/labels/`)
      .then(response => {
        commit('mutationLabels', response.data.results)
      })
  } catch (e) {
    console.log(e)
  }
}

export async function loadLabel ({ commit }, slug) {
  try {
    await axios.get(`${this.getters.getServerURL}/shop/label/${slug}/`)
      .then(response => {
        commit('mutationLabel', response.data)
        commit('mutationProducts', response.data.products)
      })
  } catch (e) {
    console.log(e)
  }
}

export async function loadProducts ({ commit }, query) {
  try {
    await axios.get(`${this.getters.getServerURL}/shop/products/?${query}`)
      .then(response => {
        commit('mutationProducts', response.data.results)
      })
  } catch (e) {
    console.log(e)
  }
}

export async function init ({ dispatch, commit }) {
  return Promise.all([
    dispatch('loadLabels'),
    dispatch('loadHotProducts')
  ]).then(() => {
    commit('mutationIsInit')
  })
}
