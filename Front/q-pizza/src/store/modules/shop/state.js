export default function () {
  return {
    isInit: false,
    labels: [],
    label: {},
    products: [],
    categories: [],
    category: {},
    hotProducts: [],
    product: {
      totalPrice: 0,
      additionsTotalPrice: 0,
      selectedAdditions: []
    },
    showProductDetail: false
  }
}
