export function mutationIsInit (state) {
  state.isInit = true
}

export function mutationHomeSlides (state, data) {
  state.homeSlides = data
}

export function mutationAStores (state, data) {
  state.aStores = data
}

export function mutationMainInfo (state, data) {
  state.mainInfo = data
}

export function mutationAboutInfo (state, data) {
  state.about = data
}

export function mutationPhotoGallery (state, data) {
  state.photoGallery = data
}

export function mutationInfoPages (state, data) {
  state.infoPages = data
}

export function mutationInfoPage (state, data) {
  state.infoPage = data
}
