import axios from 'axios'

export async function loadHomeSlides ({ commit }) {
  try {
    await axios.get(`${this.getters.getServerURL}/main/slides/`)
      .then(response => commit('mutationHomeSlides', response.data.results))
  } catch (e) {
    console.log(e)
  }
}

export async function loadMainInfo ({ commit }) {
  try {
    await axios.get(`${this.getters.getServerURL}/main/main_info/`)
      .then(response => {
        commit('mutationMainInfo', response.data.results[0])
      })
  } catch (e) {
    console.log(e)
  }
}

export async function loadInfoPages ({ commit }) {
  try {
    await axios.get(`${this.getters.getServerURL}/main/info_pages/`)
      .then(response => {
        commit('mutationInfoPages', response.data.results)
      })
  } catch (e) {
    console.log(e)
  }
}

export async function loadInfoPage ({ commit }, slug) {
  try {
    await axios.get(`${this.getters.getServerURL}/main/info_page/${slug}/`)
      .then(response => {
        commit('mutationInfoPage', response.data)
      })
  } catch (e) {
    console.log(e)
  }
}

export async function loadPhotoGallery ({ commit }) {
  try {
    await axios.get(`${this.getters.getServerURL}/main/photo_gallery/`)
      .then(response => {
        commit('mutationPhotoGallery', response.data.results)
      })
  } catch (e) {
    console.log(e)
  }
}

export async function loadAboutInfo ({ commit }) {
  try {
    await axios.get(`${this.getters.getServerURL}/main/about/`)
      .then(response => commit('mutationAboutInfo', response.data.results[0]))
  } catch (e) {
    console.log(e)
  }
}

export async function loadAStores ({ commit }) {
  try {
    await axios.get(`${this.getters.getServerURL}/main/stores/`)
      .then(response => commit('mutationAStores', response.data.results))
  } catch (e) {
    console.log(e)
  }
}

export async function init ({ dispatch, commit }) {
  return Promise.all([
    dispatch('loadHomeSlides'),
    dispatch('loadAStores'),
    dispatch('loadInfoPages')
  ])
    .then(() => {
      commit('mutationIsInit')
    })
}
