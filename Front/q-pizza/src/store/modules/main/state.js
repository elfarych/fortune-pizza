export default function () {
  return {
    isInit: false,
    homeSlides: [],
    aStores: [],
    mainInfo: {},
    about: {},
    photoGallery: {},
    infoPages: [],
    infoPage: {}
  }
}
