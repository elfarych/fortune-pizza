import { store } from 'quasar/wrappers'
import Vuex from 'vuex'
import main from './modules/main'
import shop from './modules/shop'
import wishlist from './modules/wishlist'
import cart from './modules/cart'
import blog from './modules/blog'

// import example from './module-example';
// import { ExampleStateInterface } from './module-example/state';

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */
export default store(function ({ Vue }) {
  Vue.use(Vuex)

  const Store = new Vuex.Store({
    state: {
      // serverURL: 'http://192.168.0.199:8000'
      serverURL: 'https://pizza.kz.na4u.ru'
    },
    getters: {
      getServerURL: state => state.serverURL
    },
    modules: {
      main,
      shop,
      wishlist,
      cart,
      blog
    },

    strict: !!process.env.DEBUGGING
  })

  return Store
})
