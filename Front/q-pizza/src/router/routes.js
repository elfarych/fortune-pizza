
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },
  {
    path: '/:slug',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'shop', component: () => import('pages/page-shop') }
    ]
  },
  {
    path: '/label/:slug',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'label', component: () => import('pages/page-label') }
    ]
  },
  {
    path: '/product/:slug',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'product-detail', component: () => import('pages/page-product-detail') }
    ]
  },
  {
    path: '/shop/cart',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'cart', component: () => import('pages/page-cart') }
    ]
  },
  {
    path: '/shop/wishlist',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'wishlist', component: () => import('pages/page-wishlist') }
    ]
  },
  {
    path: '/shop/about',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'about', component: () => import('pages/page-about') }
    ]
  },
  {
    path: '/shop/contacts',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'contacts', component: () => import('pages/page-contacts') }
    ]
  },
  {
    path: '/shop/thanks',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'thanks', component: () => import('pages/page-thanks') }
    ]
  },
  {
    path: '/info/:slug',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'info-page', component: () => import('pages/page-info') }
    ]
  },
  {
    path: '/blog/posts',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'blog', component: () => import('pages/blog/posts-list') }
    ]
  },
  {
    path: '/blog/posts/:slug',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'blog-detail', component: () => import('pages/blog/post-detail') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
