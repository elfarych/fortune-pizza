import { Notify } from 'quasar'

export default function (message, color = 'deep-purple-11', position = 'top') {
  Notify.create({
    classes: 'shadow-0',
    message: message,
    color: color,
    position: position,
    actions: [{ icon: 'close', color: 'white', dense: true }]
  })
}
