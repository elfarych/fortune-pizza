
export default {
  products: [],
  isInit: false,
  len: 0,
  sum: 0,

  init: function () {
    this.products = localStorage.getItem('wishlist') ? JSON.parse(localStorage.getItem('wishlist')) : []
    this.isInit = true
    this.getLen()
    this.getSum()
  },

  add: function (product) {
    this.products.push(product)
    localStorage.setItem('wishlist', JSON.stringify(this.products))
    this.update()
  },
  delete: function (id) {
    const productIndex = this.products.findIndex(item => item.id === id)
    this.products.splice(productIndex, 1)
    localStorage.setItem('wishlist', JSON.stringify(this.products))
  },
  getLen: function () {
    this.len = this.products.length
  },
  getSum: function () {
    let sum = 0
    this.products.forEach(item => {
      sum += item.price
    })
    this.sum = sum
  },

  update () {
    this.getLen()
    this.getSum()
  }
}
