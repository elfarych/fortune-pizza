export default function priceKzFilter (price) {
  return new Intl.NumberFormat('ru-ru', {
    style: 'currency',
    currency: 'KZT'
  }).format(price)
}
