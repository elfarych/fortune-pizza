from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField
from ckeditor_uploader.fields import RichTextUploadingField
from django.conf import settings


class Category(models.Model):
    title = models.CharField('Название категории', max_length=200)
    page_h1_tag = models.CharField('H1 страцниы', max_length=255, null=True, blank=True)
    slug = models.SlugField('Slug', unique=True)
    image = ThumbnailerImageField('Фото', upload_to='categories/', resize_source={'size': (300, 300), 'crop': 'scale'})
    tag_title = models.CharField('Заголовок (тег title)', max_length=120,
                                 help_text='Будет использоваться как тег title в поисковой выдаче')
    tag_description = models.TextField('Описание (тег description)', max_length=300,
                                       help_text='Будет использоваться как тег description в поисковой выдаче')
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True,
                                             help_text='Необходим для сортировки')
    public = models.BooleanField('Опубликовать', default=True)
    date = models.DateTimeField('Создано', auto_now_add=True)
    update = models.DateTimeField('Обновлено', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ('order', '-date')


class Label(models.Model):
    title = models.CharField('Название метки', max_length=200)
    slug = models.SlugField('Slug', unique=True)
    image = ThumbnailerImageField('Фото', upload_to='categories/', resize_source={'size': (500, 500), 'crop': 'scale'})
    tag_title = models.CharField('Заголовок (тег title)', max_length=120,
                                 help_text='Будет использоваться как тег title в поисковой выдаче')
    tag_description = models.TextField('Описание (тег description)', max_length=200,
                                       help_text='Будет использоваться как тег description в поисковой выдаче')
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True,
                                             help_text='Необходим для сортировки')
    public = models.BooleanField('Опубликовать', default=True)
    date = models.DateTimeField('Создано', auto_now_add=True)
    update = models.DateTimeField('Обновлено', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Метка'
        verbose_name_plural = 'Метки'
        ordering = ('order', '-date')


class Ingredient(models.Model):
    title = models.CharField('Название ингридиента', max_length=200)
    miniature = ThumbnailerImageField('Миниатюра', null=True, blank=True, upload_to='ingredients_miniature/',
                                      resize_source={'size': (200, 200), 'crop': 'scale'})
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True,
                                             help_text='Необходим для сортировки')
    date = models.DateTimeField('Создано', auto_now_add=True)
    update = models.DateTimeField('Обновлено', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Ингридиент'
        verbose_name_plural = 'Ингридиенты'
        ordering = ('order', '-date')


class VariationType(models.Model):
    title = models.CharField('Название', max_length=200, help_text='Например, размер')
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True,
                                             help_text='Необходим для сортировки')
    date = models.DateTimeField('Создано', auto_now_add=True)
    update = models.DateTimeField('Обновлено', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Тип вариация'
        verbose_name_plural = 'Типы вариаций'
        ordering = ('order', '-date')


class ProductAddition(models.Model):
    title = models.CharField('Название', max_length=100)
    image = ThumbnailerImageField('Фото', upload_to='additions/', resize_source={'size': (200, 200), 'crop': 'scale'})
    price = models.DecimalField('Цена', max_digits=10, decimal_places=0)
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True,
                                             help_text='Необходим для сортировки')
    date = models.DateTimeField('Создано', auto_now_add=True)
    update = models.DateTimeField('Обновлено', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Дополнение к товару'
        verbose_name_plural = 'Дополнения к товару'
        ordering = ('order', '-date')


class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Категория',
                                 related_name='products')
    title = models.CharField('Название', max_length=200)
    slug = models.SlugField('Slug', unique=True)
    title_tag = models.CharField('Tag title', max_length=255, null=True, blank=True)
    description_tag = models.TextField('Tag description', max_length=255, null=True, blank=True)
    price = models.DecimalField('Цена', max_digits=10, decimal_places=0)
    total_price = models.DecimalField('', max_digits=10, decimal_places=0, null=True, blank=True)
    miniature = ThumbnailerImageField('Миниатюра', upload_to='products_miniature/',
                                      resize_source={'size': (200, 200), 'crop': 'scale'})
    hit = models.BooleanField('Хит', default=False)
    public = models.BooleanField('Опубликовать', default=True)
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True,
                                             help_text='Необходим для сортировки')
    description = models.TextField('Описание')
    labels = models.ManyToManyField(Label, blank=True, verbose_name='Метки', related_name='products')
    additions = models.ManyToManyField(ProductAddition, blank=True, verbose_name='Дополнения', related_name='products')
    ingredients = models.ManyToManyField(Ingredient, blank=True, verbose_name='Ингридиенты', related_name='products')
    cooking = RichTextUploadingField('Процесс приготовления', null=True, blank=True)
    date = models.DateTimeField('Создано', auto_now_add=True)
    update = models.DateTimeField('Обновлено', auto_now=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return f'{settings.SITE_URL}/#/product/{self.slug}'

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
        ordering = ('order', '-date')


class ProductImage(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name='Товар', related_name='images')
    image = ThumbnailerImageField('Фото', upload_to='product_images/',
                                  resize_source={'size': (700, 700), 'crop': 'scale'})
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True,
                                             help_text='Необходим для сортировки')
    date = models.DateTimeField('Создано', auto_now_add=True)
    update = models.DateTimeField('Обновлено', auto_now=True)

    def __str__(self):
        return f'{self.pk}'

    class Meta:
        verbose_name = 'Фото'
        verbose_name_plural = 'Фото'
        ordering = ('order', '-date')


class Variation(models.Model):
    type = models.ForeignKey(VariationType, on_delete=models.CASCADE, verbose_name='Тип', related_name='variations')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name='Товар', related_name='variations')
    value = models.CharField('Значение', max_length=200)
    price = models.DecimalField('Цена', max_digits=10, decimal_places=0)
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True,
                                             help_text='Необходим для сортировки')
    date = models.DateTimeField('Создано', auto_now_add=True)
    update = models.DateTimeField('Обновлено', auto_now=True)

    def __str__(self):
        return self.value

    class Meta:
        verbose_name = 'Вариация'
        verbose_name_plural = 'Вариации'
        ordering = ('order',)
