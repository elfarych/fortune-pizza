from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics

from .models import Category, Label, Product
from .serializers import CategoryListSerializer, CategoryDetailSerializer, LabelListSerializer, LabelDetailSerializer, \
    ProductsListSerializer, ProductDetailSerializer

from .service import ProductsPagination, ProductsFilter


class CategoryListView(generics.ListAPIView):
    queryset = Category.objects.filter(public=True)
    serializer_class = CategoryListSerializer


class CategoryDetailView(generics.RetrieveAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoryDetailSerializer
    lookup_field = 'slug'


class LabelListView(generics.ListAPIView):
    queryset = Label.objects.filter(public=True)
    serializer_class = LabelListSerializer


class LabelDetailView(generics.RetrieveAPIView):
    queryset = Label.objects.filter(public=True)
    serializer_class = LabelDetailSerializer
    lookup_field = 'slug'


class ProductListView(generics.ListAPIView):
    serializer_class = ProductsListSerializer
    pagination_class = ProductsPagination
    filter_backends = [DjangoFilterBackend]
    filterset_class = ProductsFilter

    def get_queryset(self):
        queryset = Product.objects.filter(public=True)
        return queryset


class ProductDetailView(generics.RetrieveAPIView):
    queryset = Product.objects.filter(public=True)
    serializer_class = ProductDetailSerializer
    lookup_field = 'slug'



