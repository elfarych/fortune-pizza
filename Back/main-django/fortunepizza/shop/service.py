from django_filters import rest_framework as filters
from rest_framework.pagination import PageNumberPagination

from .models import Product


class ProductsPagination(PageNumberPagination):
    page_size = 100
    max_page_size = 100


class CharFilterInFilter(filters.BaseInFilter, filters.CharFilter):
    pass


class ProductsFilter(filters.FilterSet):
    category = CharFilterInFilter(field_name='category__slug')
    labels = CharFilterInFilter(field_name='labels_id')
    title = CharFilterInFilter(field_name='title', lookup_expr='in')
    hit = filters.BooleanFilter()

    class Meta:
        model = Product
        fields = ['category', 'title', 'hit', 'labels']
