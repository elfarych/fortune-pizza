from rest_framework import serializers
from .models import Category, Label, Ingredient, VariationType, Variation, Product, ProductAddition, ProductImage


class CategoryListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('id', 'title', 'slug', 'image', 'tag_description')


class LabelListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Label
        fields = ('id', 'title', 'slug', 'image')


class IngredientsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Ingredient
        fields = ('id', 'title', 'miniature')


class VariationsSerializer(serializers.ModelSerializer):
    type = serializers.SlugRelatedField(slug_field='title', read_only=True, many=False)

    class Meta:
        model = Variation
        fields = ('id', 'type', 'value', 'price')


class ProductAdditionsSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductAddition
        fields = ('id', 'title', 'image', 'price')


class ProductImagesSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductImage
        fields = ('id', 'image')


class ProductsListSerializer(serializers.ModelSerializer):
    ingredients = serializers.SlugRelatedField(slug_field='title', many=True, read_only=True)

    class Meta:
        model = Product
        fields = ('id', 'title', 'slug', 'price', 'hit', 'price', 'miniature', 'ingredients')


class ProductDetailSerializer(serializers.ModelSerializer):
    category = CategoryListSerializer(read_only=True, many=False)
    labels = LabelListSerializer(read_only=True, many=True)
    additions = ProductAdditionsSerializer(many=True, read_only=True)
    images = ProductImagesSerializer(read_only=True, many=True)
    variations = VariationsSerializer(many=True, read_only=True)
    ingredients = IngredientsSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        exclude = ('order', 'public', 'date', 'update')


class CategoryDetailSerializer(serializers.ModelSerializer):
    products = ProductsListSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        exclude = ('date', 'update', 'public', 'order')


class LabelDetailSerializer(serializers.ModelSerializer):
    products = ProductsListSerializer(many=True, read_only=True)

    class Meta:
        model = Label
        exclude = ('date', 'update', 'public', 'order')
