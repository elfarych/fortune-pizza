from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Category, Label, Ingredient, VariationType, ProductAddition, Product, ProductImage, Variation


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('get_image', 'title', 'public', 'order', 'date', 'update')
    list_editable = ('public', 'order')
    prepopulated_fields = {'slug': ('title',)}
    list_filter = ('public', 'date', 'update')
    search_fields = ('title',)
    save_as = True
    save_on_top = True

    def get_image(self, obj):
        if obj.image:
            return mark_safe(
                f'<img src={obj.image.url} style="height: 50px; width: 50px; object-fit: cover; border-radius: 5px;">')

    get_image.short_description = 'Фото'


@admin.register(Label)
class LabelAdmin(admin.ModelAdmin):
    list_display = ('get_image', 'title', 'public', 'order', 'date', 'update')
    list_editable = ('public', 'order')
    prepopulated_fields = {'slug': ('title',)}
    list_filter = ('public', 'date', 'update')
    search_fields = ('title',)
    save_as = True
    save_on_top = True

    def get_image(self, obj):
        if obj.image:
            return mark_safe(
                f'<img src={obj.image.url} style="height: 50px; width: 50px; object-fit: cover; border-radius: 5px;">')

    get_image.short_description = 'Фото'


@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    list_display = ('get_image', 'title', 'order', 'date', 'update')
    list_editable = ('order',)
    list_filter = ('date', 'update')
    search_fields = ('title',)
    save_as = True
    save_on_top = True

    def get_image(self, obj):
        if obj.miniature:
            return mark_safe(f'<img src={obj.miniature.url} style="height: 50px; width: 50px; object-fit: cover; border-radius: 5px;">')

    get_image.short_description = 'Фото'


@admin.register(VariationType)
class VariationTypeAdmin(admin.ModelAdmin):
    list_display = ('title', 'order', 'date', 'update')
    list_editable = ('order',)
    list_filter = ('date', 'update')
    search_fields = ('title',)
    save_as = True
    save_on_top = True


@admin.register(ProductAddition)
class ProductAdditionAdmin(admin.ModelAdmin):
    list_display = ('get_image', 'title', 'price', 'order', 'date', 'update')
    list_editable = ('price', 'order')
    list_filter = ('date', 'update')
    search_fields = ('title',)
    save_as = True
    save_on_top = True

    def get_image(self, obj):
        if obj.image:
            return mark_safe(
                f'<img src={obj.image.url} style="height: 50px; width: 50px; object-fit: cover; border-radius: 5px;">')

    get_image.short_description = 'Фото'


class ProductImageInline(admin.TabularInline):
    model = ProductImage
    extra = 0


class ProductVariationInline(admin.TabularInline):
    model = Variation
    extra = 0


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('get_image', 'title', 'price', 'order', 'hit', 'public', 'date', 'update')
    list_editable = ('price', 'order', 'public', 'hit')
    search_fields = ('title', 'category__title')
    list_filter = ('category', 'labels', 'public', 'hit', 'date', 'update')
    filter_horizontal = ('labels', 'additions', 'ingredients')
    prepopulated_fields = {'slug': ('title',)}
    inlines = [ProductImageInline, ProductVariationInline]

    save_as = True
    save_on_top = True

    def get_image(self, obj):
        if obj.miniature:
            return mark_safe(
                f'<img src={obj.miniature.url} style="height: 50px; width: 50px; object-fit: cover; border-radius: 5px;">')

    get_image.short_description = 'Фото'

