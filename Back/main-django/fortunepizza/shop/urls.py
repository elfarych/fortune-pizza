from django.urls import path
from . import views


urlpatterns = [
    path('categories/', views.CategoryListView.as_view()),
    path('category/<slug:slug>/', views.CategoryDetailView.as_view()),
    path('labels/', views.LabelListView.as_view()),
    path('label/<slug:slug>/', views.LabelDetailView.as_view()),
    path('products/', views.ProductListView.as_view()),
    path('product/<slug:slug>/', views.ProductDetailView.as_view())
]