# Generated by Django 3.2.5 on 2021-07-24 17:28

from django.db import migrations
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0002_alter_product_options'),
    ]

    operations = [
        migrations.AddField(
            model_name='ingredient',
            name='miniature',
            field=easy_thumbnails.fields.ThumbnailerImageField(blank=True, null=True, upload_to='ingredients_miniature/', verbose_name='Миниатюра'),
        ),
    ]
