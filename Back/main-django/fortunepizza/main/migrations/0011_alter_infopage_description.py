# Generated by Django 3.2.5 on 2021-10-13 18:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_maininfo_home_page_h1_tag'),
    ]

    operations = [
        migrations.AlterField(
            model_name='infopage',
            name='description',
            field=models.TextField(max_length=300, verbose_name='Краткое описание'),
        ),
    ]
