from rest_framework import serializers
from .models import MainInfo, AboutInfo, InfoPage, Slide, AStore, PhotoGallery


class PhotoGallerySerializer(serializers.ModelSerializer):

    class Meta:
        model = PhotoGallery
        fields = '__all__'


class MainInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = MainInfo
        fields = '__all__'


class AboutInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = AboutInfo
        fields = '__all__'


class InfoPageListSerializer(serializers.ModelSerializer):

    class Meta:
        model = InfoPage
        fields = ('id', 'title', 'slug')


class InfoPageDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = InfoPage
        exclude = ('public', 'order', 'date', 'update')


class SlideSerializer(serializers.ModelSerializer):
    class Meta:
        model = Slide
        exclude = ('public', 'order', 'date', 'update')


class AStoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = AStore
        fields = '__all__'
