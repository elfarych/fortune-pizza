from rest_framework import generics

from .models import MainInfo, AboutInfo, InfoPage, Slide, AStore, PhotoGallery
from .serializers import MainInfoSerializer, AboutInfoSerializer, InfoPageListSerializer, InfoPageDetailSerializer, \
    SlideSerializer, AStoreSerializer, PhotoGallerySerializer


class PhotoGalleryView(generics.ListAPIView):
    serializer_class = PhotoGallerySerializer
    queryset = PhotoGallery.objects.filter(public=True)


class MainInfoView(generics.ListAPIView):
    """Основная информация для сайта"""
    serializer_class = MainInfoSerializer
    queryset = MainInfo.objects.all()[:1]


class AboutView(generics.ListAPIView):
    """Информация о магазине"""
    serializer_class = AboutInfoSerializer
    queryset = AboutInfo.objects.all()[:1]


class AStoreListView(generics.ListAPIView):
    serializer_class = AStoreSerializer
    queryset = AStore.objects.all()


class InfoPageListView(generics.ListAPIView):
    """Инормационные страницы (список)"""
    serializer_class = InfoPageListSerializer
    queryset = InfoPage.objects.filter(public=True)


class InfoPageDetailView(generics.RetrieveAPIView):
    """Инормационная страница (детали)"""
    serializer_class = InfoPageDetailSerializer
    queryset = InfoPage.objects.filter(public=True)
    lookup_field = 'slug'


class SlideListView(generics.ListAPIView):
    """Слайды для главной"""
    queryset = Slide.objects.filter(public=True)
    serializer_class = SlideSerializer

