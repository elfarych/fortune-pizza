from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField
from ckeditor_uploader.fields import RichTextUploadingField


class MainInfo(models.Model):
    site_title = models.CharField('Заголовок сайта (tag title)', max_length=200)
    description = models.TextField('Описание сайта (tag description)', max_length=200)
    home_page_h1_tag = models.CharField('H1 главная страцница', max_length=255, null=True, blank=True)
    logo = ThumbnailerImageField('Логотип', upload_to='logo/', resize_source={'size': (400, 400), 'crop': 'scale'})
    phone = models.CharField('Номер телефона', max_length=70, help_text='в любом формате')
    whatsapp = models.CharField('Номер whatsapp', max_length=30, help_text='7707*******')
    delivery_price = models.IntegerField('Стоимость доставки', null=True, blank=True)

    def __str__(self):
        return self.site_title

    class Meta:
        verbose_name = 'Основная информация'
        verbose_name_plural = 'Основная информация'


class AboutInfo(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    description = models.TextField('Краткое описание', max_length=200)
    text = RichTextUploadingField('Полное описание')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'О магазине'
        verbose_name_plural = 'О магазине'


class PhotoGallery(models.Model):
    image = ThumbnailerImageField('Фото', upload_to='photo_gallery/',
                                  resize_source={'size': (1000, 1000), 'crop': 'scale'})
    public = models.BooleanField('Опубликовать', default=True)
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True,
                                             help_text='Необходим для сортировки')
    date = models.DateTimeField('Создано', auto_now_add=True)
    update = models.DateTimeField('Обновлено', auto_now=True)

    class Meta:
        verbose_name = 'Фотогалерея'
        verbose_name_plural = 'Фотогалерея'
        ordering = ('order', '-date')


class InfoPage(models.Model):
    title = models.CharField('Заголовок страницы', max_length=255)
    slug = models.SlugField('Slug страницы', unique=True)
    description = models.TextField('Краткое описание', max_length=300)
    text = RichTextUploadingField('Контент')
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True,
                                             help_text='Необходим для сортировки')
    public = models.BooleanField('Опубликовать', default=True)
    date = models.DateTimeField('Создано', auto_now_add=True)
    update = models.DateTimeField('Обновлено', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Информационная страница'
        verbose_name_plural = 'Информационные страницы'
        ordering = ('order', '-date')


class Slide(models.Model):
    title = models.CharField('Заголок слайда', max_length=100)
    image = ThumbnailerImageField('Фото', upload_to='slides/', resize_source={'size': (500, 500), 'crop': 'scale'})
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True,
                                             help_text='Необходим для сортировки')
    url = models.CharField('url', max_length=255, null=True, blank=True)
    public = models.BooleanField('Опубликовать', default=True)
    date = models.DateTimeField('Создано', auto_now_add=True)
    update = models.DateTimeField('Обновлено', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Слайд'
        verbose_name_plural = 'Слайдер'
        ordering = ('order', '-date')


class AStore (models.Model):
    title = models.CharField('Название', max_length=100)
    phone = models.CharField('Телефон', max_length=20, default='+7')
    address = models.CharField('Адрес', max_length=255, null=True, blank=True)
    description = RichTextUploadingField('Описание')
    map_frame = models.TextField('Карта (frame)')
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True,
                                             help_text='Необходим для сортировки')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Торговая точка'
        verbose_name_plural = 'Торговые точки'
        ordering = ('order',)


class PrivacyPolicy(models.Model):
    title = models.CharField('Заголовок', default='Политика конфиденциальности', max_length=255)
    text = RichTextUploadingField('Контент')
    date = models.DateTimeField('Создано', auto_now_add=True)
    update = models.DateTimeField('Обновлено', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Политика конфиденциальности'
        verbose_name_plural = 'Политика конфиденциальности'
