from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import MainInfo, AboutInfo, InfoPage, Slide, AStore, PhotoGallery

admin.site.register(MainInfo)
admin.site.register(AboutInfo)


@admin.register(InfoPage)
class InfoPageAdmin(admin.ModelAdmin):
    list_display = ('title', 'public', 'order', 'date', 'update')
    list_editable = ('public', 'order')
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ('title',)
    list_filter = ('public', 'date', 'update')
    save_as = True
    save_on_top = True


@admin.register(Slide)
class SlideAdmin(admin.ModelAdmin):
    list_display = ('get_image', 'title', 'public', 'order', 'date', 'update')
    list_editable = ('public', 'order')
    search_fields = ('title',)
    list_filter = ('public', 'date', 'update')
    save_as = True
    save_on_top = True

    def get_image(self, obj):
        return mark_safe(
            f'<img src={obj.image.url} style="height: 50px; width: 50px; object-fit: cover; border-radius: 5px;">')

    get_image.short_description = 'Фото'


@admin.register(PhotoGallery)
class PhotoGalleryAdmin(admin.ModelAdmin):
    list_display = ('get_image', 'public', 'order', 'date', 'update')
    list_editable = ('public', 'order')
    list_filter = ('public', 'date', 'update')
    save_as = True
    save_on_top = True

    def get_image(self, obj):
        return mark_safe(
            f'<img src={obj.image.url} style="height: 50px; width: 50px; object-fit: cover; border-radius: 5px;">')

    get_image.short_description = 'Фото'


@admin.register(AStore)
class AStoreAdmin(admin.ModelAdmin):
    list_display = ('title', 'phone', 'order')
    list_editable = ('order',)
    save_as = True
    save_on_top = True


admin.site.site_title = 'Fortune pizza'
admin.site.site_header = 'Fortune pizza - администрирование'
