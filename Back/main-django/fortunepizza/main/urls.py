from django.urls import path
from . import views


urlpatterns = [
    path('slides/', views.SlideListView.as_view()),
    path('stores/', views.AStoreListView.as_view()),
    path('main_info/', views.MainInfoView.as_view()),
    path('about/', views.AboutView.as_view()),
    path('info_pages/', views.InfoPageListView.as_view()),
    path('info_page/<slug>/', views.InfoPageDetailView.as_view()),
    path('photo_gallery/', views.PhotoGalleryView.as_view())
]
