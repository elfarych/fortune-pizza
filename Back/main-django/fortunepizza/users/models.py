from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField


class User(models.Model):
    uid = models.CharField('uid', max_length=255)
    name = models.CharField('Имя', max_length=255, null=True, blank=True)
    phone = models.CharField('Телефон', max_length=255, null=True, blank=True)
    address = models.TextField('Адрес', max_length=255, null=True, blank=True)
    date = models.DateTimeField('Создано', auto_now_add=True)
    update = models.DateTimeField('Обновлено', auto_now=True)

    def __str__(self):
        return self.name or self.uid

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
        ordering = ('-date',)


