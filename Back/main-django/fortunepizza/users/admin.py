from django.contrib import admin
from .models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'date')
    list_filter = ('date', 'update')
    search_fields = ('name', 'uid', 'phone', 'address')

    save_as = True
    save_on_top = True
