from django.urls import path
from . import views


urlpatterns = [
    path('user/<int:pk>/', views.UserView.as_view()),
    path('create_user/', views.CreateUserView.as_view()),
    path('update_user/<int:pk>/', views.UpdateUserView.as_view())
]
