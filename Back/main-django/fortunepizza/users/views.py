from rest_framework import generics
from .models import User
from .serialziers import UserSerializer


class UserView(generics.RetrieveAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()


class CreateUserView(generics.CreateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()


class UpdateUserView(generics.UpdateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()

