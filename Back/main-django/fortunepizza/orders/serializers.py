from rest_framework import serializers
from .models import Order, QuickOrder


class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = '__all__'


class QuickOrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = QuickOrder
        fields = '__all__'
