from django.db import models
from users.models import User


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Пользователь',
                             related_name='orders')
    name = models.CharField('Имя покупателя', max_length=255)
    phone = models.CharField('Номер телефона', max_length=20)
    address = models.CharField('Адрес доставки', max_length=255, null=True, blank=True)
    comment = models.TextField('Комментарий к заказу', max_length=500, null=True, blank=True)
    sum = models.DecimalField('Сумма заказа', decimal_places=2, max_digits=13)
    in_work = models.BooleanField('В работе', default=False)
    completed = models.BooleanField('Выполнен', default=False)
    payment_method = models.CharField('Способ оплаты', max_length=255, null=True, blank=True)
    platform = models.CharField('Девайс', max_length=100, null=True, blank=True)
    mobile = models.BooleanField('Телефон', null=True, blank=True)
    date = models.DateTimeField('Дата заказа', auto_now_add=True)
    update = models.DateTimeField('Обновлен', auto_now_add=True)

    def __str__(self):
        return f'#{self.pk}'

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'
        ordering = ('-date',)


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True, blank=True, related_name='items',
                              verbose_name='Товар в заказе')
    title = models.CharField('Название товара', max_length=255)
    quantity = models.PositiveSmallIntegerField('Количество')
    price = models.DecimalField('Сумма', decimal_places=2, max_digits=13)
    additionally = models.CharField('Дополнительно', max_length=255, null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Товар в заказе'
        verbose_name_plural = 'Товары в заказе'


class QuickOrder(models.Model):
    phone = models.CharField('Номер телефона', max_length=20)
    comment = models.TextField('Комментарий', null=True, blank=True)
    in_work = models.BooleanField('В работе', default=False)
    completed = models.BooleanField('Выполнен', default=False)
    date = models.DateTimeField('Дата заказа', auto_now_add=True)
    update = models.DateTimeField('Обновлен', auto_now_add=True)

    def __str__(self):
        return f'#{self.pk}'

    class Meta:
        verbose_name = 'Быстрый заказ'
        verbose_name_plural = 'Быстрые заказы'
        ordering = ('-date',)
