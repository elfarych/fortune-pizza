from django.urls import path
from . import views


urlpatterns = [
    path('create_order/', views.CreateOrderView.as_view()),
    path('create_quick_order/', views.CreateQuickOrderView.as_view())
]
