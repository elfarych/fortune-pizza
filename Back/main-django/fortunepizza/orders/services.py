import telebot
from .models import OrderItem

bot = telebot.TeleBot('1935821600:AAHdbbZNR6dHPSOCGRhXn4L9eO_TB_tmZM4')
chat_id = '-1001567603610'


def tg_send_quick_order(phone):
    text = f'===== Быстрый заказ ===== \n\n' \
           f'Номер телефона: +7{phone} \n' \

    try:
        bot.send_message(chat_id, text=text)
    except:
        return


def tg_send_order(order):
    name = order.name
    phone = order.phone
    address = order.address
    comment = order.comment
    order_sum = order.sum
    order_items = OrderItem.objects.filter(order_id=order.id)
    payment_method = order.payment_method
    device = order.platform

    text = f'===== Новый заказ ===== \n\n' \
           f'Имя: {name} \n' \
           f'Телефон: +7{phone} \n' \
           f'Адрес: {address} \n' \
           f'Способ оплаты: {payment_method} \n' \
           f'Обшая сумма: {order_sum}тг \n\n' \
           f'Комментарий: {comment} \n\n' \
           f'Устройство: {device} \n\n' \
           '===== Товары в заказе ===== \n\n' \

    for i in order_items:
        text_order = f"{i.title} \n" \
                     f"Количество: {i.quantity} \n" \
                     f"Общая стоимость: {i.price}тг \n" \
                     f"Дополнительно: {i.additionally} \n" \
                     "================= \n\n"

        text += text_order

    try:
        bot.send_message(chat_id, text=text)
    except:
        return
