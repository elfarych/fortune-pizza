from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Order, QuickOrder, OrderItem
from .serializers import OrderSerializer, QuickOrderSerializer
from .services import tg_send_order, tg_send_quick_order


class CreateOrderView(APIView):
    def post(self, request):
        order = Order.objects.create(
            name=request.data['name'],
            phone=request.data['phone'],
            address=request.data['address'],
            comment=request.data['comment'],
            sum=request.data['sum'],
            platform=request.data['platform'],
            mobile=request.data['isMobile'],
            payment_method=request.data['payment_method'],
        )
        for item in request.data['items']:
            additions = []
            for addition in item['selectedAdditions']:
                additions.append(f'{addition["title"]} {addition["price"]}тг')

            try:
                OrderItem.objects.create(
                    order=order,
                    title=f"{item['title']} ({item['selectedVariation']['value']})" or f"{item['title']}",
                    quantity=item['quantity'],
                    price=int(item['total_price']) * int(item['quantity']),
                    additionally=', '.join(additions)
                )
            except:
                OrderItem.objects.create(
                    order=order,
                    title=f"{item['title']}",
                    quantity=item['quantity'],
                    price=int(item['total_price']) * int(item['quantity']),
                    additionally=', '.join(additions)
                )

        tg_send_order(order)

        return Response(status=201)


class CreateQuickOrderView(generics.CreateAPIView):
    queryset = QuickOrder.objects.all()
    serializer_class = QuickOrderSerializer

    def perform_create(self, serializer):
        order = serializer.save()
        tg_send_quick_order(order.phone)

