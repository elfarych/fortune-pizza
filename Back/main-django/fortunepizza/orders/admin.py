from django.contrib import admin

from .models import Order, QuickOrder, OrderItem


class OrderItemsInline(admin.TabularInline):
    model = OrderItem
    extra = 0
    fields = ('title', 'price', 'quantity', 'additionally')
    readonly_fields = ('title', 'price', 'quantity', 'additionally')


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'phone', 'in_work', 'completed', 'platform', 'date')
    list_display_links = ('id', 'name')
    list_editable = ('in_work', 'completed')
    search_fields = ('name', 'phone', 'id', 'comment')
    list_filter = ('in_work', 'completed', 'platform', 'mobile', 'payment_method', 'date', 'update')
    inlines = [OrderItemsInline]
    readonly_fields = ('mobile',)


@admin.register(QuickOrder)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'phone', 'in_work', 'completed', 'date')
    list_display_links = ('id', 'phone')
    list_editable = ('in_work', 'completed')
    search_fields = ('phone', 'id', 'comment')
    list_filter = ('in_work', 'completed', 'date', 'update')
