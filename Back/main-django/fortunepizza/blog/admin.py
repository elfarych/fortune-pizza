from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Post, PostPhotoGallery


class PostPhotosInline(admin.TabularInline):
    model = PostPhotoGallery
    extra = 0


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('get_image', 'title', 'public', 'date', 'update')
    list_editable = ('public',)
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ('title',)
    list_filter = ('public', 'show_on_home_page', 'date', 'update')
    inlines = [PostPhotosInline]
    save_as = True
    save_on_top = True

    def get_image(self, obj):
        if obj.miniature:
            return mark_safe(
                f'<img src={obj.miniature.url} style="height: 50px; width: 50px; object-fit: cover; border-radius: 5px;">')

    get_image.short_description = 'Фото'
