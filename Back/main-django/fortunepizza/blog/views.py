from rest_framework import generics

from .models import Post, PostPhotoGallery
from .serializers import PostListSerializer, PostPhotosSerializer, PostDetailSerializer


class PostsListView(generics.ListAPIView):
    serializer_class = PostListSerializer
    queryset = Post.objects.filter(public=True)


class PostDetailView(generics.RetrieveAPIView):
    queryset = Post.objects.filter(public=True)
    serializer_class = PostDetailSerializer
    lookup_field = 'slug'
