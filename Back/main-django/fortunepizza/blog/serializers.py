from rest_framework import serializers

from .models import Post, PostPhotoGallery


class PostListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = ('id', 'title', 'miniature', 'slug', 'show_on_home_page')


class PostPhotosSerializer(serializers.ModelSerializer):

    class Meta:
        model = PostPhotoGallery
        fields = '__all__'


class PostDetailSerializer(serializers.ModelSerializer):
    images = PostPhotosSerializer(many=True, read_only=True)

    class Meta:
        model = Post
        exclude = ('public', 'date', 'update')

