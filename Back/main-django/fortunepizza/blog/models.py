from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from easy_thumbnails.fields import ThumbnailerImageField


class Post(models.Model):
    title = models.CharField('Заголовок поста', max_length=255)
    title_tag = models.CharField('Tag title', max_length=255, null=True, blank=True)
    description_tag = models.TextField('Tag description', max_length=255, null=True, blank=True)
    slug = models.SlugField('', unique=True)
    show_on_home_page = models.BooleanField('Опубликовать на главной', default=False)
    public = models.BooleanField('Опубликовать', default=True)
    miniature = ThumbnailerImageField('Миниатюра', upload_to='post_images/',
                                      resize_source={'size': (400, 400), 'crop': 'scale'})
    description = models.TextField('Краткое описание', null=True, blank=True)
    content = RichTextUploadingField()
    date = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'
        ordering = ('-date',)


class PostPhotoGallery(models.Model):
    post = models.ForeignKey(Post, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Пост',
                             related_name='images')
    image = ThumbnailerImageField('Фото', upload_to='post_images/',
                                  resize_source={'size': (1000, 1000), 'crop': 'scale'})
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True)

    def __str__(self):
        return f'{self.pk}'

    class Meta:
        verbose_name = 'Фото'
        verbose_name_plural = 'Фото'
        ordering = ('order',)
